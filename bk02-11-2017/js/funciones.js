$( document ).ready(function() {

  precarga_home();
  mapa();
  btn_tiendas();
  fancys();

});


$( window ).resize(function() {	

});






function precarga_home(){

  var imageload = {
    1 : 'images/fondo_home.jpg',
    2 : 'images/pop-up.png', 
  };

  var loader = new PxLoader();

  for( pos in imageload )
  {
    var pxImage = new PxLoaderImage( imageload[pos] );
    loader.add(pxImage);
  }

  loader.addProgressListener(function(e){
    var percentage = Math.ceil( ( e.completedCount / e.totalCount ) * 100 );
  });

  loader.addCompletionListener(function(){
    $('#loader').fadeOut("slow");
    if ($(window).width() > 720) {
      $(".fancybox a").eq(0).trigger('click');
    }


    var timer=false
    timer = setTimeout(function(){
      /*animacion_home();*/
    }, 10)

  });
  //layer_devise(); 
  loader.start();
}



function mapa(){
  $map = new GMaps({
    div: '#ubication_map',
    lat: -12.080888,
    lng: -77.011485,
    scrollwheel: false,
  });
  $map.addMarker({
    lat: -12.080888,
    lng: -77.011485,
    title: 'Hara',
    icon: 'images/ubication_check.png',
    infoWindow: {
      content: '<p>Fermín Tanguis 160, Santa Catalina - La Victoria </p>'
    }
  });
}


function btn_tiendas(){
  $(".btn_tiendas").on({
    click: function(){
      $(".mapa_tiendas").fadeIn();
    }
  })
}



function fancys(){
  $(".fancybox a").fancybox({
    padding : 0,
    
    afterClose: function() {
      $('html, body').animate({scrollTop:$('#formulario').position().top}, 'slow');
      $('#formulario').focus();
    }
  });

  
  console.log("layer");

  $("#infoLayer").on({
    click:function(){
      $('html, body').animate({scrollTop:$('#formulario').position().top}, 'slow');
      $.fancybox.close();
    }
  });
  $(".btn-deja-tus-datos").on({
    click:function(){
      $('html, body').animate({scrollTop:$('#formulario').position().top}, 'slow');
      $.fancybox.close();
    }
  });


}
